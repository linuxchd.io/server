# Server Installation Guide

This is how I set up my mac server at home with linux.

## Prerequisites

- server
- network
- usb
- image

## Download Image 

Download Debian 11 titled `firmware-testing-amd64-netinst.iso` from the [Official Website](https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/weekly-builds/amd64/iso-cd/).

## Flash image to USB

If you're on a linux distribution, you can use the `dd` command.

```sh
$ dd bs=4M if=/path/to/image of=/dev/sdx status=progress oflag=sync && sync
```
> Note that you need to edit `if=/path/to/image` and `of=/dev/sdx`.

### Using usbimager

1. clone the repository 

```sh
git clone https://aur.archlinux.org/usbimager.git
```
2. install package 

```sh
makepkg -si
```
## Booting Image

Press `alt` to enter boot menu and select device image to boot.

## Installation of Image

Select `install`.

Watch the video tutorial from [Chris Titus Tech](https://www.youtube.com/watch?v=szbN-g0FMC8&t=921s) for further installation.

## Setting up the server

1. Update the server's repository and installing essential tools

```sh
apt install && apt upgrade -y
apt install sudo vim git
```

2. Set up new user

```sh
adduser mac
```

3. Set up password for both user and root

```sh
passwd
passwd mac
```

4. Edit sudoers file to grant sudo permissions

```sh
sudo vim /etc/sudoers
```
```sh
mac     ALL=(ALL:ALL) ALL
%sudo   ALL=(ALL:ALL) NOPASSWD: ALL
```

5. Switch to user

```sh
su mac
```
